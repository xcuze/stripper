
Pod::Spec.new do |s|
  s.name             = "Stripper"
  s.version          = "0.1.0"
  s.summary          = "HTML Stripping without blocking the UI Thread"

  s.description      = <<-DESC
                        Strips HTML tags from a String using an NSXMLParser so that the UI is not blocked (as it is
                        when using Webkit for XML parsing).
                       DESC

  s.homepage         = "https://bitbucket.org/xcuze/stripper"
  s.license          = 'MIT'
  s.author           = { "Florian Krüger" => "florian.krueger@projectserver.org" }
  s.source           = { :git => "https://bitbucket.org/xcuze/stripper.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
end
