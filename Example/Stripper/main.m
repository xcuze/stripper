//
//  main.m
//  Stripper
//
//  Created by Florian Krüger on 02/18/2016.
//  Copyright (c) 2016 Florian Krüger. All rights reserved.
//

@import UIKit;
#import "STRPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([STRPAppDelegate class]));
    }
}
