//
//  STRPAppDelegate.h
//  Stripper
//
//  Created by Florian Krüger on 02/18/2016.
//  Copyright (c) 2016 Florian Krüger. All rights reserved.
//

@import UIKit;

@interface STRPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
