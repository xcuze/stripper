# Stripper

[![CI Status](http://img.shields.io/travis/Florian Krüger/Stripper.svg?style=flat)](https://travis-ci.org/Florian Krüger/Stripper)
[![Version](https://img.shields.io/cocoapods/v/Stripper.svg?style=flat)](http://cocoapods.org/pods/Stripper)
[![License](https://img.shields.io/cocoapods/l/Stripper.svg?style=flat)](http://cocoapods.org/pods/Stripper)
[![Platform](https://img.shields.io/cocoapods/p/Stripper.svg?style=flat)](http://cocoapods.org/pods/Stripper)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Stripper is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Stripper"
```

## Author

Florian Krüger, florian.krueger@projectserver.org

## License

Stripper is available under the MIT license. See the LICENSE file for more info.
