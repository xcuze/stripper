//
//  NSString+DecodeHTMLEntities.m
//  Stripper
//
//  Created by Florian Krüger
//  Copyright © 2016 projectserver.org. All rights reserved.
//
//  Based on the work of Leigh McCulloch from 2011. Released under the MIT license.
//  See: https://gist.github.com/leighmcculloch/1202238
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "NSString+DecodeHTMLEntities.h"

@implementation NSString (DecodeHTMLEntities)

+ (NSDictionary *)STRP_HTMLToUnicodeDictionary
{
  static NSDictionary *STRP_HTMLToUnicodeDictionary = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    STRP_HTMLToUnicodeDictionary =
    @{@"&amp;": @"&",
      @"&Ouml;": @"Ö",
      @"&ouml;": @"ö",
      @"&Uuml;": @"Ü",
      @"&uuml;": @"ü",
      @"&Auml;": @"Ä",
      @"&auml;": @"ä",
      @"&quot;": @"\"",
      @"&apos;": @"'",
      @"&nbsp;": @" ",
      @"&iexcl;": @"¡",
      @"&cent;": @"¢",
      @"&pound;": @"£",
      @"&curren;": @"¤",
      @"&yen;": @"¥",
      @"&brvbar;": @"¦",
      @"&sect;": @"§",
      @"&uml;": @"¨",
      @"&copy;": @"©",
      @"&ordf;": @"ª",
      @"&laquo;": @"«",
      @"&not;": @"¬",
      @"&shy;": @"­ ",
      @"&reg;": @"®",
      @"&macr;": @"¯",
      @"&deg;": @"°",
      @"&plusmn;": @"±",
      @"&sup2;": @"²",
      @"&sup3;": @"³",
      @"&acute;": @"´",
      @"&micro;": @"µ",
      @"&para;": @"¶",
      @"&middot;": @"·",
      @"&cedil;": @"¸",
      @"&sup1;": @"¹",
      @"&ordm;": @"º",
      @"&raquo;": @"»",
      @"&frac14;": @"¼",
      @"&frac12;	": @"½",
      @"&frac34;": @"¾",
      @"&iquest;": @"¿",
      @"&Agrave;": @"À",
      @"&Aacute;": @"Á",
      @"&Acirc;": @"Â",
      @"&Atilde;": @"Ã",
      @"&Aring;": @"Å",
      @"&AElig;": @"Æ",
      @"&Ccedil;": @"Ç",
      @"&Egrave;": @"È",
      @"&Eacute;": @"É",
      @"&Ecirc;": @"Ê",
      @"&Euml;": @"Ë",
      @"&Igrave;": @"Ì",
      @"&Iacute;": @"Í",
      @"&Icirc;": @"Î",
      @"&Iuml;": @"Ï",
      @"&ETH;": @"Ð",
      @"&Ntilde;": @"Ñ",
      @"&Ograve;": @"Ò",
      @"&Oacute;": @"Ó",
      @"&Ocirc;": @"Ô",
      @"&Otilde;": @"Õ",
      @"&Ouml;": @"Ö",
      @"&times;": @"×",
      @"&Oslash;": @"Ø",
      @"&Ugrave;": @"Ù",
      @"&Uacute;": @"Ú",
      @"&Ucirc;": @"Û",
      @"&Uuml;": @"Ü",
      @"&Yacute;": @"Ý",
      @"&THORN;": @"Þ",
      @"&szlig;": @"ß",
      @"&agrave;": @"à",
      @"&aacute;": @"á",
      @"&acirc;": @"â",
      @"&atilde;": @"ã",
      @"&auml;": @"ä",
      @"&aring;": @"å",
      @"&aelig;": @"æ",
      @"&ccedil;": @"ç",
      @"&egrave;": @"è",
      @"&eacute;": @"é",
      @"&ecirc;": @"ê",
      @"&euml;": @"ë",
      @"&igrave;": @"ì",
      @"&iacute;": @"í",
      @"&icirc;": @"î",
      @"&iuml;": @"ï",
      @"&eth;": @"ð",
      @"&ntilde;": @"ñ",
      @"&ograve;": @"ò",
      @"&oacute;": @"ó",
      @"&ocirc;": @"ô",
      @"&otilde;": @"õ",
      @"&ouml;": @"ö",
      @"&divide;": @"÷",
      @"&oslash;": @"ø",
      @"&ugrave;": @"ù",
      @"&uacute;": @"ú",
      @"&ucirc;": @"û",
      @"&uuml;": @"ü",
      @"&yacute;": @"ý",
      @"&thorn;": @"þ",
      @"&yuml;": @"ÿ"
      };
  });
  return STRP_HTMLToUnicodeDictionary;
}

- (NSString *)STRP_decodeHTMLEntities
{
  NSDictionary *dict = [[self class] STRP_HTMLToUnicodeDictionary];

  NSRange range = NSMakeRange(0, [self length]);
  NSRange subrange = [self rangeOfString:@"&" options:NSBackwardsSearch range:range];

  // if no ampersands, we've got a quick way out
  if (subrange.length == 0) return self;
  NSMutableString *finalString = [NSMutableString stringWithString:self];
  do {
    NSRange semiColonRange = NSMakeRange(subrange.location, NSMaxRange(range) - subrange.location);
    semiColonRange = [self rangeOfString:@";" options:0 range:semiColonRange];
    range = NSMakeRange(0, subrange.location);
    // if we don't find a semicolon in the range, we don't have a sequence
    if (semiColonRange.location == NSNotFound) {
      continue;
    }
    NSRange escapeRange = NSMakeRange(subrange.location, semiColonRange.location - subrange.location + 1);
    NSString *escapeString = [self substringWithRange:escapeRange];
    NSUInteger length = [escapeString length];
    // a squence must be longer than 3 (&lt;) and less than 11 (&thetasym;)
    if (length > 3 && length < 11) {
      if ([escapeString characterAtIndex:1] == '#') {
        unichar char2 = [escapeString characterAtIndex:2];
        if (char2 == 'x' || char2 == 'X') {
          // Hex escape squences &#xa3;
          NSString *hexSequence = [escapeString substringWithRange:NSMakeRange(3, length - 4)];
          NSScanner *scanner = [NSScanner scannerWithString:hexSequence];
          unsigned value;
          if ([scanner scanHexInt:&value] &&
              value < USHRT_MAX &&
              value > 0
              && [scanner scanLocation] == length - 4) {
            unichar uchar = (unichar)value;
            NSString *charString = [NSString stringWithCharacters:&uchar length:1];
            [finalString replaceCharactersInRange:escapeRange withString:charString];
          }

        } else {
          // Decimal Sequences &#123;
          NSString *numberSequence = [escapeString substringWithRange:NSMakeRange(2, length - 3)];
          NSScanner *scanner = [NSScanner scannerWithString:numberSequence];
          int value;
          if ([scanner scanInt:&value] &&
              value < USHRT_MAX &&
              value > 0
              && [scanner scanLocation] == length - 3) {
            unichar uchar = (unichar)value;
            NSString *charString = [NSString stringWithCharacters:&uchar length:1];
            [finalString replaceCharactersInRange:escapeRange withString:charString];
          }
        }
      } else {
        // "standard" sequences
        NSString *replacementString = [dict objectForKey:escapeString];
        if (nil != replacementString) {
          [finalString replaceCharactersInRange:escapeRange withString:replacementString];
        }
      }
    }
  } while ((subrange = [self rangeOfString:@"&" options:NSBackwardsSearch range:range]).length != 0);
  return finalString;
}

@end
