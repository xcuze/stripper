//
//  STRPStripHTMLParser.m
//  Stripper
//
//  Created by Florian Krüger
//  Copyright © 2016 projectserver.org. All rights reserved.
//
//  Based on the work of Leigh McCulloch from 2011. Released under the MIT license.
//  See: https://gist.github.com/leighmcculloch/1202238
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "STRPStripHTMLParser.h"

@interface STRPStripHTMLParser ()

@property (nonnull, nonatomic, strong, readwrite) NSMutableArray *strings;

@end

@implementation STRPStripHTMLParser

+ (instancetype)parser
{
    return [[STRPStripHTMLParser alloc] init];
}

- (id)init
{
    self = [super init];
    if(self) {
        _strings = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    [_strings addObject:string];
}

- (NSString *)getCharsFound
{
    return [_strings componentsJoinedByString:@""];
}

@end
