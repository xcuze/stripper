//
//  NSString+StripHTML.m
//  Stripper
//
//  Created by Florian Krüger
//  Copyright © 2016 projectserver.org. All rights reserved.
//
//  Based on the work of Leigh McCulloch from 2011. Released under the MIT license.
//  See: https://gist.github.com/leighmcculloch/1202238
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "NSString+StripHTML.h"
#import "STRPStripHTMLParser.h"

@implementation NSString (StripHTML)

- (NSString *)STRP_stripHtml
{
    // take this string obj and wrap it in a root element to ensure only a single root element exists
    // and that any ampersands are escaped to preserve the escaped sequences
    NSString* string = [self stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    string = [NSString stringWithFormat:@"<root>%@</root>", string];

    // add the string to the xml parser
    NSStringEncoding encoding = string.fastestEncoding;
    NSData* data = [string dataUsingEncoding:encoding];
    NSXMLParser* xmlParser = [[NSXMLParser alloc] initWithData:data];

    // parse the content keeping track of any chars found outside tags (this will be the stripped content)
    STRPStripHTMLParser* stripHTMLParser = [STRPStripHTMLParser parser];
    xmlParser.delegate = stripHTMLParser;
    [xmlParser parse];

    // any chars found while parsing are the stripped content
    NSString* strippedString = [stripHTMLParser getCharsFound];

    // get the raw text out of the stripHTMLParser after parsing, and return it
    return strippedString;
}

@end
